#ifndef C63_CUDA_COMMON_H_
#define C63_CUDA_COMMON_H_

#include <inttypes.h>

#include "c63.h"


//void gpu_dequantize_idct_row(int16_t *in_data, uint8_t *prediction, int *w, int *h,
//	uint8_t *out_data, uint8_t *quantization);

void gpu_dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
                         uint32_t height, uint8_t *out_data, uint8_t *quantization, int component, int mb_rows, int mb_cols);

//void gpu_dct_quantize_row(uint8_t *in_data, uint8_t *prediction, int *w, int *h,
//                          int16_t *out_data, uint8_t *quantization);

void gpu_dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
                      uint32_t height, int16_t *out_data, uint8_t *quantization, int component, int mb_rows, int mb_cols);

#endif /* C63_CUDA_COMMON_H_ */
