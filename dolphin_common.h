#ifndef __DOLPHIN_COMMON__H
#define __DOLPHIN_COMMON__H

#define GROUP 7
#include "c63.h"
#define GET_SEGMENT_ID(id) ( GROUP << 16 | id )

#define SEGMENT_CLIENT GET_SEGMENT_ID(1)
#define SEGMENT_SERVER GET_SEGMENT_ID(2)

void sci_check_error(const char * pretty_function, sci_error_t error, const char * file, const char * function);

#define CHECK_ERROR(last_function) sci_check_error(last_function, error, __FILE__, __func__)

#define PRINT_COLOR_RESET "\x1B[0m"

#define PRINT(...) printf(PRINT_COLOR); printf(__VA_ARGS__); printf(PRINT_COLOR_RESET)

#define DMA_READY     7
#define ENCODE_READY     8
#define NO_FLAGS      0
#define DATA_SIZE   522240//4966*100

// Interrupts
enum {
    INT_SERVER_TO_CLIENT,
    INT_CLIENT_TO_SERVER
};

// Signals (client to server)
enum {
    SIG_CONTINUE,
    SIG_FINISHED
};

extern size_t image_size;
extern uint32_t ypw, yph, upw, uph, vpw, vph;
extern unsigned int image_width, image_height;

void init_sizes(int width, int height);

// Interrupts
enum {
 SERVER_TO_CLIENT,
 CLIENT_TO_SERVER
};

#endif // __DOLPHIN_COMMON__H
