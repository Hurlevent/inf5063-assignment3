#ifndef C63_CUDA_DSP_H_
#define C63_CUDA_DSP_H_

#define ISQRT2 0.70710678118654f

#include <inttypes.h>

__device__ void gpu_dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
                                        uint8_t *quant_tbl);

__device__ void gpu_dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
                                           uint8_t *quant_tbl);


#endif  /* C63_CUDA_DSP_H_ */
