#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "io.h"

void put_byte(struct c63_common *cm, int byte)
{

  uint8_t val = (uint8_t)byte;
  int buffLength = cm->buffer_width;
  cm->dataBuffer[buffLength] = val;
  cm->buffer_width += 1;
}

void put_bytes(struct c63_common *cm, const void* data, unsigned int len)
{
  int buffLength = cm->buffer_width;
  memcpy(cm->dataBuffer + buffLength, data, len);
  cm->buffer_width += len;
}

uint8_t get_byte(FILE *fp)
{
  int status = fgetc(fp);

  if (status == EOF)
  {
    fprintf(stderr, "End of file.\n");
    exit(EXIT_FAILURE);
  }

  return (uint8_t) status;
}

int read_bytes(FILE *fp, void *data, unsigned int sz)
{
  size_t status = fread(data, 1, (size_t) sz, fp);

  if ((int) status == EOF)
  {
    fprintf(stderr, "End of file.\n");
    exit(EXIT_FAILURE);
  }
  else if (status != (size_t) sz)
  {
    fprintf(stderr, "Error reading bytes\n");
    exit(EXIT_FAILURE);
  }

  return (int) status;
}

/**
 * Adds a bit to the bitBuffer. A call to bit_flush() is needed
 * in order to write any remainding bits in the buffer before
 * writing using another function.
 */
void put_bits(struct c63_common *cm, uint16_t bits, uint8_t n)
{
	struct entropy_ctx *c = &(cm->e_ctx);
  assert(n <= 24  && "Error writing bit");

  if(n == 0) { return; }

  c->bit_buffer <<= n;
  c->bit_buffer |= bits & ((1 << n) - 1);
  c->bit_buffer_width += n;

  while(c->bit_buffer_width >= 8)
  {
    uint8_t b = (uint8_t)(c->bit_buffer >> (c->bit_buffer_width - 8));

    put_byte(cm, b);

    if(b == 0xff) { put_byte(cm, 0); }

    c->bit_buffer_width -= 8;
  }
}

uint16_t get_bits(struct entropy_ctx *c, uint8_t n)
{
  uint16_t ret = 0;

  while(c->bit_buffer_width < n)
  {
    uint8_t b = get_byte(c->fp);
    if (b == 0xff) { get_byte(c->fp); } /* Discard stuffed byte */

    c->bit_buffer <<= 8;
    c->bit_buffer |= b;
    c->bit_buffer_width += 8;
  }

  ret = c->bit_buffer >> (c->bit_buffer_width - n);
  c->bit_buffer_width -= n;

  /* Clear grabbed bits */
  c->bit_buffer &= (1 << c->bit_buffer_width) - 1;

  return ret;
}

/**
 * Flushes the bitBuffer by writing zeroes to fill a full byte
 */
void flush_bits(struct c63_common *cm)
{
	struct entropy_ctx *c = &(cm->e_ctx);
  if(c->bit_buffer > 0)
  {
    uint8_t b = c->bit_buffer << (8 - c->bit_buffer_width);
    put_byte(cm, b);

    if(b == 0xff) { put_byte(cm, 0); }
  }

  c->bit_buffer = 0;
  c->bit_buffer_width = 0;
}
